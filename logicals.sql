-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 10, 2013 at 09:53 PM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `logicals`
--

-- --------------------------------------------------------

--
-- Table structure for table `eplan`
--

CREATE TABLE IF NOT EXISTS `eplan` (
  `eplanID` varchar(32) NOT NULL,
  `func_text` varchar(32) NOT NULL,
  `address` varchar(32) NOT NULL,
  `signal_number` varchar(32) NOT NULL,
  `commit_id` tinyint(4) NOT NULL,
  `change_type` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`eplanID`),
  KEY `commit` (`commit_id`),
  KEY `change_type` (`change_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eplan`
--


-- --------------------------------------------------------

--
-- Table structure for table `eplan_changes`
--

CREATE TABLE IF NOT EXISTS `eplan_changes` (
  `changeID` int(11) NOT NULL AUTO_INCREMENT,
  `eplanID` varchar(32) NOT NULL,
  `func_text` varchar(32) NOT NULL,
  `address` varchar(32) NOT NULL,
  `signal_number` varchar(32) NOT NULL,
  `commit_id` tinyint(4) NOT NULL,
  `change_type` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`changeID`),
  KEY `commit` (`commit_id`),
  KEY `change_type` (`change_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `eplan_changes`
--


-- --------------------------------------------------------

--
-- Table structure for table `opm`
--

CREATE TABLE IF NOT EXISTS `opm` (
  `opmID` varchar(32) NOT NULL,
  `GRP` varchar(8) NOT NULL,
  `SYS` varchar(8) NOT NULL,
  `EQP` varchar(8) NOT NULL,
  `SIG` varchar(8) NOT NULL,
  `long_text` varchar(32) NOT NULL,
  `KOMP` varchar(8) NOT NULL,
  `BSE` varchar(8) NOT NULL,
  `SSE` varchar(8) NOT NULL,
  `commit_id` tinyint(4) NOT NULL,
  `change_type` tinyint(1) NOT NULL,
  PRIMARY KEY (`opmID`),
  KEY `commit` (`commit_id`),
  KEY `change_type` (`change_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opm`
--


-- --------------------------------------------------------

--
-- Table structure for table `opm_changes`
--

CREATE TABLE IF NOT EXISTS `opm_changes` (
  `changeID` int(11) NOT NULL AUTO_INCREMENT,
  `opmID` varchar(32) NOT NULL,
  `GRP` varchar(8) NOT NULL,
  `SYS` varchar(8) NOT NULL,
  `EQP` varchar(8) NOT NULL,
  `SIG` varchar(8) NOT NULL,
  `long_text` varchar(32) NOT NULL,
  `KOMP` varchar(8) NOT NULL,
  `BSE` varchar(8) NOT NULL,
  `SSE` varchar(8) NOT NULL,
  `commit_id` tinyint(4) NOT NULL,
  `change_type` tinyint(1) NOT NULL,
  PRIMARY KEY (`changeID`),
  KEY `commit` (`commit_id`),
  KEY `change_type` (`change_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `opm_changes`
--


-- --------------------------------------------------------

--
-- Table structure for table `vcdm`
--

CREATE TABLE IF NOT EXISTS `vcdm` (
  `vcdmID` varchar(32) NOT NULL,
  `kks0` varchar(8) NOT NULL,
  `kks1` varchar(8) NOT NULL,
  `kks2` varchar(8) NOT NULL,
  `kks3` varchar(8) NOT NULL,
  `long_text` varchar(32) NOT NULL,
  `comp_number` varchar(8) NOT NULL,
  `cpu_number` varchar(8) NOT NULL,
  `rack_id` varchar(8) NOT NULL,
  `commit_id` tinyint(4) NOT NULL,
  `change_type` tinyint(1) NOT NULL,
  PRIMARY KEY (`vcdmID`),
  KEY `change_type` (`change_type`),
  KEY `commit` (`commit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vcdm`
--


-- --------------------------------------------------------

--
-- Table structure for table `vcdm_changes`
--

CREATE TABLE IF NOT EXISTS `vcdm_changes` (
  `changeID` int(11) NOT NULL AUTO_INCREMENT,
  `vcdmID` varchar(32) NOT NULL,
  `kks0` varchar(8) NOT NULL,
  `kks1` varchar(8) NOT NULL,
  `kks2` varchar(8) NOT NULL,
  `kks3` varchar(8) NOT NULL,
  `long_text` varchar(32) NOT NULL,
  `comp_number` varchar(8) NOT NULL,
  `cpu_number` varchar(8) NOT NULL,
  `rack_id` varchar(8) NOT NULL,
  `commit_id` tinyint(4) NOT NULL,
  `change_type` tinyint(1) NOT NULL,
  PRIMARY KEY (`changeID`),
  KEY `change_type` (`change_type`),
  KEY `commit` (`commit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `vcdm_changes`
--

