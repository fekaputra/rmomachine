SELECT ?change_type (count(?s) as ?change_found)
WHERE {
 ?s a vocab:eplan
.?s vocab:eplan_change_type ?change_type
.?s vocab:eplan_commit_id ?xxx
} GROUP BY ?change_type
ORDER BY ?change_type 

SELECT ?change_type (count(?s) as ?change_found)
WHERE {
 ?s a vocab:eplan_changes
.?s vocab:eplan_changes_change_type ?change_type
.?s vocab:eplan_changes_commit_id ?xxx
} GROUP BY ?change_type
ORDER BY ?change_type 
