package com.mkyong;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.mkyong.helper.Helper;
import com.mkyong.helper.Helper.CHANGE_TYPE;
import com.mkyong.logicals.Eplan;
import com.mkyong.logicals.EplanChanges;
import com.mkyong.logicals.Opm;
import com.mkyong.logicals.OpmChanges;
import com.mkyong.logicals.Vcdm;
import com.mkyong.logicals.VcdmChanges;
import com.mkyong.util.HibernateUtil;

/**
 * this class runs the first scenario of Richard's comparison paper
 * 
 * @author Juang
 *
 */
public class RMoMachineV5 {
	// TODO: logger
	private String directory = "rmomachine";
	private int maxEntry = 100;
	private Map<Integer, Map<String, String>> fileMap;
	private OntoLib olib;
	private CSVWriter csvWriter;
	private StringBuffer tempWriter;
	private MemoryMXBean memoryMXBean;
	
	public RMoMachineV5() {
		initMachine();
		initCSVWriter();
		olib = new OntoLib();
		memoryMXBean = ManagementFactory.getMemoryMXBean();
        System.out.println(memoryMXBean.getHeapMemoryUsage().getUsed());
	}
	
	/**
	 * init the CSV writer
	 */
	private void initCSVWriter()
	{
        try {
            String header = "cid#" +
            		"insEpl#mi1(kb)#insProp#mi2(kb)#insTotal#" +
            		"updEpl#mu1(kb)#updProp#mu2(kb)#updTotal#" +
            		"delEpl#md1(kb)#delProp#md2(kb)#delTotal#" +
            		"dbsize(kb)" +
            		"#q1#m1(kb)#q2#m2(kb)#q3#m3(kb)#q4#m4(kb)#q5#m5(kb)#q6#m6(kb)#q7#m7(kb)#q8#m8(kb)";
            String[] entries = header.split("#");
            csvWriter = new CSVWriter(new FileWriter("result.csv"), ';', CSVWriter.NO_QUOTE_CHARACTER);
            csvWriter.writeNext(entries);
        } catch (IOException e) {
            e.printStackTrace();
        }
	    
	}
	/**
	 * read all files in the directory folder and sort it from 0-20
	 */
	private void initMachine() {
		File dir = new File(directory);
		fileMap = new HashMap<Integer, Map<String, String>>();
		for(File child : dir.listFiles()) {
			String[] filename = child.getName().split("_");
			
			Map<String, String> files;
			Integer scenario = Integer.parseInt(filename[1]);
			if(fileMap.containsKey(scenario)) {
				files = fileMap.get(scenario);
			} else {
				files = new HashMap<String, String>();
				fileMap.put(scenario, files);
			}
			String[] filenameTails = filename[2].split("\\.");
			files.put(filenameTails[0], child.getPath());
		}
	}
	
	/**
	 * run every commit from the scenario
	 */
	public void start() {
		SortedSet<Integer> keys = new TreeSet<Integer>(fileMap.keySet());
		for (Integer commit : keys) {
			Map<String, String> files = fileMap.get(commit);
			doCommit(commit, files);
		}
		try {
            csvWriter.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}

	/**
	 * Put commit into database
	 * 
	 * @param commit
	 * @param files
	 */
	private void doCommit(Integer commit, Map<String, String> files) {
        long tempTime = 0;
        
		System.out.println("Scenario "+commit+" Start!");
		tempWriter = new StringBuffer();
		tempWriter.append(commit);
		if(files.isEmpty()) return;
		if(files.containsKey("inserts")) {
			String filename = files.get("inserts");
			tempTime = doInsert(commit, filename);
			tempWriter.append("#").append(tempTime);
		}
		if(files.containsKey("updates")) {
			String filename = files.get("updates");
            tempTime = doUpdate(commit, filename);
            tempWriter.append("#").append(tempTime);
		}
		if(files.containsKey("deletes")) {
			String filename = files.get("deletes");
            tempTime = doDelete(commit, filename);
            tempWriter.append("#").append(tempTime);
		}
		tempWriter.append("#").append(Helper.getDbSizeQuery());
		tempWriter.append(olib.executeQueries(commit));
		csvWriter.writeNext(tempWriter.toString().split("#"));
	}
	
	/**
	 * do the insertion process
	 * 	including the propagation
	 * 
	 * note: here we are using sql to propagate the changes since it will be easier and faster
	 * 
	 * @param commit
	 * @param filename
	 */
	private long doInsert(Integer commit, String filename) {

		System.out.println(filename);
        long memUsage = memoryMXBean.getHeapMemoryUsage().getUsed();
		long starttime = System.currentTimeMillis();
		doInsertEplan(commit, filename);
        memUsage += memoryMXBean.getHeapMemoryUsage().getUsed();
        long endtime = System.currentTimeMillis();
        long temp1 = endtime-starttime;
        
        tempWriter.append("#").append(temp1);
        tempWriter.append("#").append(memUsage/1024/2);

        memUsage = memoryMXBean.getHeapMemoryUsage().getUsed();
        starttime = System.currentTimeMillis();
		propagateInsert(commit);
        memUsage += memoryMXBean.getHeapMemoryUsage().getUsed();
		endtime = System.currentTimeMillis();
        long temp2 = endtime-starttime;
        
        tempWriter.append("#").append(temp2);
        tempWriter.append("#").append(memUsage/1024/2);

		return temp1+temp2;
	}
	
	/**
	 * insertion process
	 * 	where we really read from the csv file and start to put things into database 
	 * 
	 * @param commit
	 * @param filename
	 */
	private void doInsertEplan(Integer commit, String filename) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			CSVReader reader = new CSVReader(new FileReader(filename), ';', '\'', 1);
			String [] nextLine;
			int i = 0;
		    while ((nextLine = reader.readNext()) != null) {
		    	i++;
		    	if(i > maxEntry) {
		    		i = 0;
		    		session.getTransaction().commit(); 
		    		session.clear();
		    		session.beginTransaction();
		    	}
				Eplan eplan = new Eplan(nextLine[0], nextLine[2], nextLine[3], nextLine[1], commit, CHANGE_TYPE.INSERT.ordinal());
				session.save(eplan);
		    }
    		session.getTransaction().commit(); 
		    reader.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Propagating the insert using sql query
	 * 
	 * @param commit
	 */
	private void propagateInsert(Integer commit) {
		String qEplanToVcdm = Helper.EPLAN_TO_VCDM_INSERT.concat(" WHERE commit_id = "+commit
				+" AND change_type = "+CHANGE_TYPE.INSERT.ordinal());
		executeQuery(qEplanToVcdm);
		String qVcdmToOpm = Helper.VCDM_TO_OPM_INSERT.concat(" WHERE commit_id = "+commit
				+" AND change_type = "+CHANGE_TYPE.INSERT.ordinal());
		executeQuery(qVcdmToOpm);
	}
	
	/**
	 * updating process
	 * 	we also count the time and print it out.
	 * 
	 * @param commit
	 * @param filename
	 */
	private long doUpdate(Integer commit, String filename) {

		System.out.println(filename);
		return doUpdateEplan(commit, filename);
	}
	
	/**
	 * the update and also the propagation process.
	 * 	I am not using any sql since it quite complicated
	 * 	So, I am relying on the hibernate for doing the update
	 * 
	 * @param commit
	 * @param filename
	 */
	private long doUpdateEplan(Integer commit, String filename) {
	    long insertTime = 0;
	    int ii = 1;
        long memUsageInsert = memoryMXBean.getHeapMemoryUsage().getUsed();
		Session sessionInsert = HibernateUtil.getSessionFactory().openSession();
        sessionInsert.beginTransaction();

        long propagateTime = 0;
        int jj = 1;
        long memUsagePropagate = memoryMXBean.getHeapMemoryUsage().getUsed();
        Session sessionPropagate = HibernateUtil.getSessionFactory().openSession();
		sessionPropagate.beginTransaction();
		
		
		try {
			CSVReader reader = new CSVReader(new FileReader(filename), ';', '\'', 1);
			String [] nextLine;
			int i = 0;
			int j = 0;
			long temp;
		    while ((nextLine = reader.readNext()) != null) {
		    	i++;
		    	j++;
		    	if(i > maxEntry) {
		    		i = 0;
		    		temp = System.currentTimeMillis();
		    		sessionInsert.getTransaction().commit(); 
                    ii++;
                    memUsageInsert += memoryMXBean.getHeapMemoryUsage().getUsed();
		    		sessionInsert.clear();
		    		sessionInsert.beginTransaction();
		    		insertTime += System.currentTimeMillis() - temp;
		    	}
                if(j > maxEntry) {
                    j = 0;
                    temp = System.currentTimeMillis();
                    sessionPropagate.getTransaction().commit(); 
                    jj++;
                    memUsagePropagate += memoryMXBean.getHeapMemoryUsage().getUsed();
                    sessionPropagate.clear();
                    sessionPropagate.beginTransaction();
                    propagateTime += System.currentTimeMillis() - temp;
                }

                temp = System.currentTimeMillis();
		    	Eplan eplan = (Eplan) sessionInsert.get(Eplan.class, nextLine[0]);
                EplanChanges eplanChanges = new EplanChanges(eplan);
                eplan.setAttributes(nextLine[0], nextLine[2], nextLine[3], nextLine[1], commit, CHANGE_TYPE.UPDATE.ordinal());
                sessionInsert.save(eplan);
                sessionInsert.save(eplanChanges);
                insertTime += System.currentTimeMillis() - temp;

                temp = System.currentTimeMillis();
		    	Vcdm vcdm = (Vcdm) sessionPropagate.get(Vcdm.class, nextLine[0]);
		    	Opm opm = (Opm) sessionPropagate.get(Opm.class, nextLine[0]);
                VcdmChanges vcdmChanges = new VcdmChanges(vcdm);
                OpmChanges opmChanges = new OpmChanges(opm);
		    	vcdm.setAttributes(eplan);
				opm.setAttributes(vcdm);
                sessionPropagate.save(vcdm);
				sessionPropagate.save(opm);
				sessionPropagate.save(vcdmChanges);
				sessionPropagate.save(opmChanges);
                propagateTime += System.currentTimeMillis() - temp;
		    }
    		sessionInsert.getTransaction().commit(); 
            tempWriter.append("#").append(insertTime);
            tempWriter.append("#").append(memUsageInsert/1024/ii);
            sessionPropagate.getTransaction().commit();
            tempWriter.append("#").append(propagateTime); 
            tempWriter.append("#").append(memUsagePropagate/1024/jj);
		    reader.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return insertTime+propagateTime;
	}
	
	/**
	 * the deletion process
	 * 	including time counter
	 * 
	 * @param commit
	 * @param filename
	 */
	private long doDelete(Integer commit, String filename) {

		System.out.println(filename);
		return doDeleteEplan(commit, filename);
	}
	
	/**
	 * similar to update, here you can find all the deletion and propagation
	 * 	I suppose the performance will be better using sql, but it need to be proved later
	 * 
	 * @param commit
	 * @param filename
	 */
	private long doDeleteEplan(Integer commit, String filename) {
        long insertTime = 0;
        int ii = 1;
        long memUsageInsert = memoryMXBean.getHeapMemoryUsage().getUsed();
        Session sessionInsert = HibernateUtil.getSessionFactory().openSession();
        sessionInsert.beginTransaction();

        long propagateTime = 0;
        int jj = 1;
        long memUsagePropagate = memoryMXBean.getHeapMemoryUsage().getUsed();
        Session sessionPropagate = HibernateUtil.getSessionFactory().openSession();
        sessionPropagate.beginTransaction();
        
        try {
            CSVReader reader = new CSVReader(new FileReader(filename), ';', '\'', 1);
            String [] nextLine;
            int i = 0;
            int j = 0;
            long temp;
            while ((nextLine = reader.readNext()) != null) {
                i++;
                j++;
                if(i > maxEntry) {
                    i = 0;
                    temp = System.currentTimeMillis();
                    sessionInsert.getTransaction().commit(); 
                    ii++;
                    memUsageInsert += memoryMXBean.getHeapMemoryUsage().getUsed();
                    sessionInsert.clear();
                    sessionInsert.beginTransaction();
                    insertTime += System.currentTimeMillis() - temp;
                }
                if(j > maxEntry) {
                    j = 0;
                    temp = System.currentTimeMillis();
                    sessionPropagate.getTransaction().commit(); 
                    jj++;
                    memUsagePropagate += memoryMXBean.getHeapMemoryUsage().getUsed();
                    sessionPropagate.clear();
                    sessionPropagate.beginTransaction();
                    propagateTime += System.currentTimeMillis() - temp;
                }

                temp = System.currentTimeMillis();
		    	Eplan eplan = (Eplan) sessionInsert.get(Eplan.class, nextLine[0]);
		    	if(eplan==null) continue;
                EplanChanges eplanChanges = new EplanChanges(eplan);
                eplan.setAttributes(nextLine[0], nextLine[2], nextLine[3], nextLine[1], commit, CHANGE_TYPE.DELETE.ordinal());
                EplanChanges eplanDel = new EplanChanges(eplan);
                insertTime += System.currentTimeMillis() - temp;

                temp = System.currentTimeMillis();
		    	Vcdm vcdm = (Vcdm) sessionPropagate.get(Vcdm.class, nextLine[0]);
		    	Opm opm = (Opm) sessionPropagate.get(Opm.class, nextLine[0]);
				VcdmChanges vcdmChanges = new VcdmChanges(vcdm);
		    	OpmChanges opmChanges = new OpmChanges(opm);
                vcdm.setAttributes(eplan);
                opm.setAttributes(vcdm);
		    	VcdmChanges vcdmDel = new VcdmChanges(vcdm);
		    	OpmChanges opmDel = new OpmChanges(opm);
		    	
		    	sessionInsert.delete(eplan);
                sessionInsert.save(eplanChanges);
                sessionInsert.save(eplanDel);
		    	sessionPropagate.delete(vcdm);
		    	sessionPropagate.delete(opm);
				sessionPropagate.save(vcdmChanges);
				sessionPropagate.save(opmChanges);
				sessionPropagate.save(vcdmDel);
				sessionPropagate.save(opmDel);
                propagateTime += System.currentTimeMillis() - temp;
		    }

            sessionInsert.getTransaction().commit(); 
            tempWriter.append("#").append(insertTime);
            tempWriter.append("#").append(memUsageInsert/1024/ii);
            sessionPropagate.getTransaction().commit();
            tempWriter.append("#").append(propagateTime); 
            tempWriter.append("#").append(memUsagePropagate/1024/jj);
            
		    reader.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return insertTime+propagateTime;
	}

	/**
	 * Native query execution
	 * 
	 * @param query
	 */
	private void executeQuery(String query) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery insQuery = session.createSQLQuery(query);
		insQuery.executeUpdate();
		session.getTransaction().commit();
	}

	/**
	 *  set heap limit (50000?)
	 *  open folder
	 *  iterate file per scenario
	 *  each scenario: inserts, updates, deletes
	 *  	print out time for each process
	 *  		breakdown for propagation
	 *  	print out volume of changeset
	 *  		breakdown for changeset
	 *  print out time for all process
	 */
	public static void main(String[] args) {
		RMoMachineV5 machine = new RMoMachineV5();
		machine.start();
	}	
}
