package com.mkyong.logicals;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@SuppressWarnings("serial")
@Entity
@Table(name = "vcdm_changes")
public class VcdmChanges implements java.io.Serializable {

	/**
	 * 
	 */
	private String changeID;
	private String vcdmID;
	private String kks0;
	private String kks1;
	private String kks2;
	private String kks3;
	private String long_text;
	private String comp_number;
	private String cpu_number;
	private String rack_id;
	private int commit_id;
	private int change_type;

	public VcdmChanges() {
	}

	public VcdmChanges(String vcdmID, String kks0, String kks1, String kks2,
			String kks3, String long_text, String comp_number,
			String cpu_number, String rack_id, int commit_id, int change_type) {
		super();
		this.vcdmID = vcdmID;
		this.kks0 = kks0;
		this.kks1 = kks1;
		this.kks2 = kks2;
		this.kks3 = kks3;
		this.long_text = long_text;
		this.comp_number = comp_number;
		this.cpu_number = cpu_number;
		this.rack_id = rack_id;
		this.commit_id = commit_id;
		this.change_type = change_type;
	}

	public VcdmChanges(Vcdm vcdm) {
		super();
		this.vcdmID = vcdm.getVcdmID();
		this.kks0 = vcdm.getKks0();
		this.kks1 = vcdm.getKks1();
		this.kks2 = vcdm.getKks2();
		this.kks3 = vcdm.getKks3();
		this.long_text = vcdm.getLong_text();
		this.comp_number = vcdm.getComp_number();
		this.cpu_number = vcdm.getCpu_number();
		this.rack_id = vcdm.getRack_id();
		this.commit_id = vcdm.getCommit_id();
		this.change_type = vcdm.getChange_type();
	}

	@Id
	@GenericGenerator(strategy="increment", name = "")
	@GeneratedValue(generator="")
	@Column(name = "changeID", unique = true, nullable = false, precision = 5, scale = 0)
	public String getChangeID() {
		return changeID;
	}

	public void setChangeID(String changeID) {
		this.changeID = changeID;
	}

	@Column(name = "vcdmID", nullable = false)
	public String getVcdmID() {
		return vcdmID;
	}

	public void setVcdmID(String vcdmID) {
		this.vcdmID = vcdmID;
	}


	@Column(name = "kks0", nullable = false, length = 8)
	public String getKks0() {
		return kks0;
	}

	public void setKks0(String kks0) {
		this.kks0 = kks0;
	}


	@Column(name = "kks1", nullable = false, length = 8)
	public String getKks1() {
		return kks1;
	}

	public void setKks1(String kks1) {
		this.kks1 = kks1;
	}


	@Column(name = "kks2", nullable = false, length = 8)
	public String getKks2() {
		return kks2;
	}

	public void setKks2(String kks2) {
		this.kks2 = kks2;
	}


	@Column(name = "kks3", nullable = false, length = 8)
	public String getKks3() {
		return kks3;
	}

	public void setKks3(String kks3) {
		this.kks3 = kks3;
	}


	@Column(name = "long_text", nullable = false, length = 32)
	public String getLong_text() {
		return long_text;
	}

	public void setLong_text(String long_text) {
		this.long_text = long_text;
	}


	@Column(name = "comp_number", nullable = false, length = 8)
	public String getComp_number() {
		return comp_number;
	}

	public void setComp_number(String comp_number) {
		this.comp_number = comp_number;
	}


	@Column(name = "cpu_number", nullable = false, length = 8)
	public String getCpu_number() {
		return cpu_number;
	}

	public void setCpu_number(String cpu_number) {
		this.cpu_number = cpu_number;
	}


	@Column(name = "rack_id", nullable = false, length = 8)
	public String getRack_id() {
		return rack_id;
	}

	public void setRack_id(String rack_id) {
		this.rack_id = rack_id;
	}
	
	@Column(name = "commit_id", nullable = false, length = 4)
	public int getCommit_id() {
		return commit_id;
	}
	
	public void setCommit_id(int commit_id) {
		this.commit_id = commit_id;
	}

	@Column(name = "change_type", nullable = false, length = 1)
	public int getChange_type() {
		return change_type;
	}

	public void setChange_type(int change_type) {
		this.change_type = change_type;
	}
	
}
