package com.mkyong.logicals;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "vcdm")
public class Vcdm implements java.io.Serializable {

	/**
	 * 
	 */
	private String vcdmID;
	private String kks0;
	private String kks1;
	private String kks2;
	private String kks3;
	private String long_text;
	private String comp_number;
	private String cpu_number;
	private String rack_id;
	private int commit_id;
	private int change_type;

	public Vcdm() {
	}

	public Vcdm(String vcdmID, String kks0, String kks1, String kks2,
			String kks3, String long_text, String comp_number,
			String cpu_number, String rack_id, int commit_id, int change_type) {
		super();
		this.vcdmID = vcdmID;
		this.kks0 = kks0;
		this.kks1 = kks1;
		this.kks2 = kks2;
		this.kks3 = kks3;
		this.long_text = long_text;
		this.comp_number = comp_number;
		this.cpu_number = cpu_number;
		this.rack_id = rack_id;
		this.commit_id = commit_id;
		this.change_type = change_type;
	}
	
	public Vcdm(Eplan eplan) {
		this.vcdmID = eplan.getEplanID();
		this.kks0 = eplan.getSignal_number().substring(0,2);
		this.kks1 = eplan.getSignal_number().substring(3,8);
		this.kks2 = eplan.getSignal_number().substring(9,14);
		this.kks3 = eplan.getSignal_number().substring(15,19);
		this.long_text = eplan.getFunc_text();
		this.comp_number = eplan.getAddress().substring(0,3);
		this.cpu_number = eplan.getAddress().substring(4,6);
		this.rack_id = eplan.getAddress().substring(7,9);
		this.commit_id = eplan.getCommit_id();
		this.change_type = eplan.getChange_type();
	}
	
	public void setAttributes(Eplan eplan) {
		this.vcdmID = eplan.getEplanID();
		this.kks0 = eplan.getSignal_number().substring(0,2);
		this.kks1 = eplan.getSignal_number().substring(3,8);
		this.kks2 = eplan.getSignal_number().substring(9,14);
		this.kks3 = eplan.getSignal_number().substring(15,19);
		this.long_text = eplan.getFunc_text();
		this.comp_number = eplan.getAddress().substring(0,3);
		this.cpu_number = eplan.getAddress().substring(4,6);
		this.rack_id = eplan.getAddress().substring(7,9);
		this.commit_id = eplan.getCommit_id();
		this.change_type = eplan.getChange_type();
	}
	
	public void setEmpty(int commit_id, int change_type) {
		this.vcdmID = "";
		this.kks0 = "";
		this.kks1 = "";
		this.kks2 = "";
		this.kks3 = "";
		this.long_text = "";
		this.comp_number = "";
		this.cpu_number = "";
		this.rack_id = "";
		this.commit_id = commit_id;
		this.change_type = change_type;
	}

	@Id
	@Column(name = "vcdmID", unique = true, nullable = false, precision = 5, scale = 0)
	public String getVcdmID() {
		return vcdmID;
	}

	public void setVcdmID(String vcdmID) {
		this.vcdmID = vcdmID;
	}


	@Column(name = "kks0", nullable = false, length = 8)
	public String getKks0() {
		return kks0;
	}

	public void setKks0(String kks0) {
		this.kks0 = kks0;
	}


	@Column(name = "kks1", nullable = false, length = 8)
	public String getKks1() {
		return kks1;
	}

	public void setKks1(String kks1) {
		this.kks1 = kks1;
	}


	@Column(name = "kks2", nullable = false, length = 8)
	public String getKks2() {
		return kks2;
	}

	public void setKks2(String kks2) {
		this.kks2 = kks2;
	}


	@Column(name = "kks3", nullable = false, length = 8)
	public String getKks3() {
		return kks3;
	}

	public void setKks3(String kks3) {
		this.kks3 = kks3;
	}


	@Column(name = "long_text", nullable = false, length = 32)
	public String getLong_text() {
		return long_text;
	}

	public void setLong_text(String long_text) {
		this.long_text = long_text;
	}


	@Column(name = "comp_number", nullable = false, length = 8)
	public String getComp_number() {
		return comp_number;
	}

	public void setComp_number(String comp_number) {
		this.comp_number = comp_number;
	}


	@Column(name = "cpu_number", nullable = false, length = 8)
	public String getCpu_number() {
		return cpu_number;
	}

	public void setCpu_number(String cpu_number) {
		this.cpu_number = cpu_number;
	}


	@Column(name = "rack_id", nullable = false, length = 8)
	public String getRack_id() {
		return rack_id;
	}

	public void setRack_id(String rack_id) {
		this.rack_id = rack_id;
	}
	
	@Column(name = "commit_id", nullable = false, length = 4)
	public int getCommit_id() {
		return commit_id;
	}
	
	public void setCommit_id(int commit_id) {
		this.commit_id = commit_id;
	}

	@Column(name = "change_type", nullable = false, length = 1)
	public int getChange_type() {
		return change_type;
	}

	public void setChange_type(int change_type) {
		this.change_type = change_type;
	}
	
}
