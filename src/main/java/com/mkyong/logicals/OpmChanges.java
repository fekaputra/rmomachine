package com.mkyong.logicals;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@SuppressWarnings("serial")
@Entity
@Table(name = "opm_changes")
public class OpmChanges implements java.io.Serializable {

	/**
	 * 
	 */
	private String changeID;
	private String opmID;
	private String GRP;
	private String SYS;
	private String EQP;
	private String SIG;
	private String long_text;
	private String KOMP;
	private String BSE;
	private String SSE;
	private int commit_id;
	private int change_type;

	public OpmChanges() {
	}

	public OpmChanges(String opmID, String gRP, String sYS, String eQP, String sIG,
			String long_text, String kOMP, String bSE, String sSE,
			int commit_id, int change_type) {
		super();
		this.opmID = opmID;
		this.GRP = gRP;
		this.SYS = sYS;
		this.EQP = eQP;
		this.SIG = sIG;
		this.long_text = long_text;
		this.KOMP = kOMP;
		this.BSE = bSE;
		this.SSE = sSE;
		this.commit_id = commit_id;
		this.change_type = change_type;
	}

	public OpmChanges(Opm opm) {
		super();
		this.opmID = opm.getOpmID();
		this.GRP = opm.getGRP();
		this.SYS = opm.getSYS();
		this.EQP = opm.getEQP();
		this.SIG = opm.getSIG();
		this.long_text = opm.getLong_text();
		this.KOMP = opm.getKOMP();
		this.BSE = opm.getBSE();
		this.SSE = opm.getSSE();
		this.commit_id = opm.getCommit_id();
		this.change_type = opm.getChange_type();
	}

	@Id
	@GenericGenerator(strategy="increment", name = "")
	@GeneratedValue(generator="")
	@Column(name = "changeID", unique = true, nullable = false, precision = 5, scale = 0)
	public String getChangeID() {
		return changeID;
	}

	public void setChangeID(String changeID) {
		this.changeID = changeID;
	}

	@Column(name = "opmID", unique = true, nullable = false, precision = 5, scale = 0)
	public String getOpmID() {
		return opmID;
	}

	public void setOpmID(String opmID) {
		this.opmID = opmID;
	}

	@Column(name = "GRP", nullable = false, length = 8)
	public String getGRP() {
		return GRP;
	}

	public void setGRP(String gRP) {
		GRP = gRP;
	}

	@Column(name = "SYS", nullable = false, length = 8)
	public String getSYS() {
		return SYS;
	}

	public void setSYS(String sYS) {
		SYS = sYS;
	}

	@Column(name = "EQP", nullable = false, length = 8)
	public String getEQP() {
		return EQP;
	}

	public void setEQP(String eQP) {
		EQP = eQP;
	}

	@Column(name = "SIG", nullable = false, length = 8)
	public String getSIG() {
		return SIG;
	}

	public void setSIG(String sIG) {
		SIG = sIG;
	}

	@Column(name = "long_text", nullable = false, length = 8)
	public String getLong_text() {
		return long_text;
	}

	public void setLong_text(String long_text) {
		this.long_text = long_text;
	}

	@Column(name = "KOMP", nullable = false, length = 8)
	public String getKOMP() {
		return KOMP;
	}

	public void setKOMP(String kOMP) {
		KOMP = kOMP;
	}

	@Column(name = "BSE", nullable = false, length = 8)
	public String getBSE() {
		return BSE;
	}

	public void setBSE(String bSE) {
		BSE = bSE;
	}

	@Column(name = "SSE", nullable = false, length = 8)
	public String getSSE() {
		return SSE;
	}

	public void setSSE(String sSE) {
		SSE = sSE;
	}

	@Column(name = "commit_id", nullable = false, length = 8)
	public int getCommit_id() {
		return commit_id;
	}

	public void setCommit_id(int commit_id) {
		this.commit_id = commit_id;
	}

	@Column(name = "change_type", nullable = false, length = 8)
	public int getChange_type() {
		return change_type;
	}

	public void setChange_type(int change_type) {
		this.change_type = change_type;
	}
	
}
