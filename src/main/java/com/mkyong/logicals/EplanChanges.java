package com.mkyong.logicals;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@SuppressWarnings("serial")
@Entity
@Table(name = "eplan_changes")
public class EplanChanges implements java.io.Serializable {

	/**
	 * 
	 */
	private String changeID;
	private String eplanID;
	private String func_text;
	private String address;
	private String signal_number;
	private int commit_id;
	private int change_type;

	public EplanChanges() {
	}

	public EplanChanges(String eplanID, String func_text, String address,
			String signal_number, int commit_id, int change_type) {
		super();
		this.eplanID = eplanID;
		this.func_text = func_text;
		this.address = address;
		this.signal_number = signal_number;
		this.commit_id = commit_id;
		this.change_type = change_type;
	}

	public EplanChanges(Eplan eplan) {
		super();
		this.eplanID = eplan.getEplanID();
		this.func_text = eplan.getFunc_text();
		this.address = eplan.getAddress();
		this.signal_number = eplan.getSignal_number();
		this.commit_id = eplan.getCommit_id();
		this.change_type = eplan.getChange_type();
	}

	@Id
	@GenericGenerator(strategy="increment", name = "")
	@GeneratedValue(generator="")
	@Column(name = "changeID", unique = true, nullable = false, precision = 5, scale = 0)
	public String getChangeID() {
		return changeID;
	}

	public void setChangeID(String changeID) {
		this.changeID = changeID;
	}

	@Column(name = "eplanID", nullable = false)
	public String getEplanID() {
		return eplanID;
	}

	public void setEplanID(String eplanID) {
		this.eplanID = eplanID;
	}

	@Column(name = "func_text", nullable = false, length = 32)
	public String getFunc_text() {
		return func_text;
	}

	public void setFunc_text(String func_text) {
		this.func_text = func_text;
	}

	@Column(name = "address", nullable = false, length = 32)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "signal_number", nullable = false, length = 32)
	public String getSignal_number() {
		return signal_number;
	}

	public void setSignal_number(String signal_number) {
		this.signal_number = signal_number;
	}
	
	@Column(name = "commit_id", nullable = false, length = 4)
	public int getCommit_id() {
		return commit_id;
	}
	
	public void setCommit_id(int commit_id) {
		this.commit_id = commit_id;
	}

	@Column(name = "change_type", nullable = false, length = 1)
	public int getChange_type() {
		return change_type;
	}

	public void setChange_type(int change_type) {
		this.change_type = change_type;
	}
	
}
