package com.mkyong.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Helper {
    public static String dbSizeQuery = 
            "SELECT SUM( data_length + index_length )/1024 \"size\" " +
            "FROM information_schema.TABLES " +
            "WHERE table_schema = \"logicals\"";
    
    public static String getDbSizeQuery() {
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
        String DB_URL = "jdbc:mysql://localhost/information_schema";
        String USER = "root";
        String PASS = "";
        String result = "";

        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql = dbSizeQuery;
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 5: Extract data from result set
            while(rs.next()){
               result = Integer.toString(rs.getInt("size"));
            }
            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
         }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
         }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
         }finally{
            //finally block used to close resources
            try{
               if(stmt!=null)
                  stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
               if(conn!=null)
                  conn.close();
            }catch(SQLException se){
               se.printStackTrace();
            }//end finally try
         }//end try
        
        
        return result;
    }
	
	public static enum CHANGE_TYPE {
		INSERT,
		UPDATE,
		DELETE
	}
	
	public final static String EPLAN_TO_VCDM_INSERT = 
			"INSERT INTO vcdm (vcdmID, kks0, kks1, kks2, kks3, long_text, comp_number, cpu_number, rack_id, commit_id, change_type) " +
			"SELECT eplanID, " +
				"SUBSTRING(signal_number, 1,2), " +
				"SUBSTRING(signal_number, 4,5), " +
				"SUBSTRING(signal_number, 10,5), " +
				"SUBSTRING(signal_number, 16,4),  " +
				"func_text, " +
				"SUBSTRING(address, 1,3), " +
				"SUBSTRING(address, 5,2), " +
				"SUBSTRING(address, 8,2), " +
				"commit_id, " +
				"change_type " +
			"FROM eplan ";
	public final static String VCDM_TO_OPM_INSERT = 
			"INSERT INTO opm (opmID, GRP, SYS, EQP, SIG, long_text, KOMP, BSE, SSE, commit_id, change_type) " +
			"SELECT vcdmID, kks0, kks1, kks2, kks3, long_text, comp_number, cpu_number, rack_id, commit_id, change_type " +
			"FROM vcdm";
	
	public static void main(String[] args) {
	    System.out.println(getDbSizeQuery());
	}
	
}
