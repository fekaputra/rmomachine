package com.mkyong;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import au.com.bytecode.opencsv.CSVWriter;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

public class OntoLib {
    CSVWriter writer = null;
    
    private String directory = "query";
    private String sparqlEndpoint = "http://localhost:2020/sparql";
    OntModel model = null;
    HashMap<Integer, Vector<ParameterizedSparqlString>> queryMap = null;
    private MemoryMXBean memoryMXBean;
    private long memSummary;
    
    public OntoLib() {
        initAttr();
    }
    
    public String executeQueries(Integer commitID) {
        Set<Integer> keys = queryMap.keySet();
        StringBuffer queryTime = new StringBuffer();
        for (Integer qNum : keys) {
            Vector<ParameterizedSparqlString> query = queryMap.get(qNum);
            Iterator<ParameterizedSparqlString> queryPart = query.iterator();
            long timecounter = 0;
            while(queryPart.hasNext()) {
                ParameterizedSparqlString qString = queryPart.next();
                qString.setParam("xxx", ResourceFactory.createPlainLiteral(Integer.toString(commitID)));
                memSummary += memoryMXBean.getHeapMemoryUsage().getUsed();
                timecounter += exec(qString);
            }
            queryTime.append("#").append(timecounter).append("#").append(memSummary/1024/4);
            memSummary = 0;
        }
        return queryTime.toString();
    }
     
    public long exec(ParameterizedSparqlString query) {
        memSummary += memoryMXBean.getHeapMemoryUsage().getUsed();
        long timestart = System.currentTimeMillis();
        QueryExecution qexec = QueryExecutionFactory.sparqlService(sparqlEndpoint, query.asQuery());
        memSummary += memoryMXBean.getHeapMemoryUsage().getUsed();
        qexec.execSelect();
        memSummary += memoryMXBean.getHeapMemoryUsage().getUsed();
        long timestop = System.currentTimeMillis();
        qexec.close();
        return timestop-timestart;
    }
    
//    public void printQueries() {
//        Set<Integer> keys = queryMap.keySet();
//        for (Integer qNum : keys) {
//            System.out.println("QUERY :: "+qNum);
//            Vector<ParameterizedSparqlString> queries = queryMap.get(qNum);
//            Iterator<ParameterizedSparqlString> iter = queries.iterator();
//            while(iter.hasNext()) {
//                System.out.println("QUERY :: "+qNum+"PART");
//                ParameterizedSparqlString query = iter.next();
//                System.out.println(query.toString());
//            }
//        }
//    }

    private void initAttr() {
        memoryMXBean = ManagementFactory.getMemoryMXBean();
        memSummary = 0;
        
        initQuery();
        model = ModelFactory.createOntologyModel();
        try {
            writer = new CSVWriter(new FileWriter("result.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private void initQuery() {
        File dir = new File(directory);
        queryMap = new HashMap<Integer, Vector<ParameterizedSparqlString>>();
        
        for(File child : dir.listFiles()) {
            String[] filename = child.getName().split("\\.");
            
            Vector<ParameterizedSparqlString> queries;
            Integer scenario = Integer.parseInt(filename[0]);
            if(queryMap.containsKey(scenario)) {
                queries = queryMap.get(scenario);
            } else {
                queries = new Vector<ParameterizedSparqlString>();
                queryMap.put(scenario, queries);
            }
            
            parseQueryFile(queries, child.getPath());
        }
    }
    
    private void parseQueryFile(Vector<ParameterizedSparqlString> queries, String path) {
        FileInputStream inFile = null;
        BufferedReader bufReader = null;
        
        try {
            inFile = new FileInputStream(path);
            bufReader = new BufferedReader(new InputStreamReader(inFile));
            ParameterizedSparqlString query = null;

            String line = bufReader.readLine();
            while(line != null) {
                line = line.trim().toLowerCase();
                if(line.startsWith("select")) {
                    query = new ParameterizedSparqlString();
                    query.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
                    query.setNsPrefix("vocab", "http://localhost:2020/resource/vocab/");
                    query.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");
                } else if(line.isEmpty()) {
                    queries.add(query);
                }
                query.append(line);
                query.append(" ");
                line = bufReader.readLine();
            }
            if(query!=null) queries.add(query);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufReader.close();
                inFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
    }
    
    public static void main(String[] args) {
        OntoLib o = new OntoLib();
        o.initQuery();
    }
}
