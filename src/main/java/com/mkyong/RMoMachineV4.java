package com.mkyong;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.mkyong.helper.Helper;
import com.mkyong.helper.Helper.CHANGE_TYPE;
import com.mkyong.logicals.Eplan;
import com.mkyong.logicals.EplanChanges;
import com.mkyong.logicals.Opm;
import com.mkyong.logicals.OpmChanges;
import com.mkyong.logicals.Vcdm;
import com.mkyong.logicals.VcdmChanges;
import com.mkyong.util.HibernateUtil;

/**
 * this class runs the first scenario of Richard's comparison paper
 * 
 * @author Juang
 *
 */
public class RMoMachineV4 {
	// TODO: logger
	private String directory = "rmomachine";
	private int maxEntry = 100;
	private Map<Integer, Map<String, String>> fileMap;
	private OntoLib olib;
	private CSVWriter csvWriter;
	private StringBuffer tempWriter;
	private MemoryMXBean memoryMXBean;
	private MemoryUsage memoryUsage;
	
	public RMoMachineV4() {
		initMachine();
		initCSVWriter();
		olib = new OntoLib();
		memoryMXBean = ManagementFactory.getMemoryMXBean();
        memoryUsage = memoryMXBean.getHeapMemoryUsage();
        System.out.println(memoryUsage.getUsed());
	}
	
	/**
	 * init the CSV writer
	 */
	private void initCSVWriter()
	{
        try {
            String[] entries = "cid#timeinsert#timeupdate#timedelete#q1#q2#q3#q4#q5#q6#q7#q8".split("#");
            csvWriter = new CSVWriter(new FileWriter("result.csv"), ';', CSVWriter.NO_QUOTE_CHARACTER);
            csvWriter.writeNext(entries);
        } catch (IOException e) {
            e.printStackTrace();
        }
	    
	}
	/**
	 * read all files in the directory folder and sort it from 0-20
	 */
	private void initMachine() {
		File dir = new File(directory);
		fileMap = new HashMap<Integer, Map<String, String>>();
		for(File child : dir.listFiles()) {
			String[] filename = child.getName().split("_");
			
			Map<String, String> files;
			Integer scenario = Integer.parseInt(filename[1]);
			if(fileMap.containsKey(scenario)) {
				files = fileMap.get(scenario);
			} else {
				files = new HashMap<String, String>();
				fileMap.put(scenario, files);
			}
			String[] filenameTails = filename[2].split("\\.");
			files.put(filenameTails[0], child.getPath());
		}
	}
	
	/**
	 * run every commit from the scenario
	 */
	public void start() {
		SortedSet<Integer> keys = new TreeSet<Integer>(fileMap.keySet());
		for (Integer commit : keys) {
			Map<String, String> files = fileMap.get(commit);
			doCommit(commit, files);
		}
		try {
            csvWriter.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}

	/**
	 * Put commit into database
	 * 
	 * @param commit
	 * @param files
	 */
	private void doCommit(Integer commit, Map<String, String> files) {
		System.out.println("Scenario "+commit+" Start!");
		tempWriter = new StringBuffer();
		tempWriter.append(commit);
		if(files.isEmpty()) return;
		if(files.containsKey("inserts")) {
			String filename = files.get("inserts");
			tempWriter.append("#").append(doInsert(commit, filename));
		}
		if(files.containsKey("updates")) {
			String filename = files.get("updates");
            tempWriter.append("#").append(doUpdate(commit, filename));
		}
		if(files.containsKey("deletes")) {
			String filename = files.get("deletes");
            tempWriter.append("#").append(doDelete(commit, filename));
		}
		tempWriter.append(olib.executeQueries(commit));
		csvWriter.writeNext(tempWriter.toString().split("#"));
	}
	
	/**
	 * do the insertion process
	 * 	including the propagation
	 * 
	 * note: here we are using sql to propagate the changes since it will be easier and faster
	 * 
	 * @param commit
	 * @param filename
	 */
	private long doInsert(Integer commit, String filename) {

		System.out.println(filename);
		long starttime = System.currentTimeMillis();
		doInsertEplan(commit, filename);
		propagateInsert(commit);
		long endtime = System.currentTimeMillis();
		long s = endtime - starttime;
		return s;
	}
	
	/**
	 * insertion process
	 * 	where we really read from the csv file and start to put things into database 
	 * 
	 * @param commit
	 * @param filename
	 */
	private void doInsertEplan(Integer commit, String filename) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			CSVReader reader = new CSVReader(new FileReader(filename), ';', '\'', 1);
			String [] nextLine;
			int i = 0;
		    while ((nextLine = reader.readNext()) != null) {
		    	i++;
		    	if(i > maxEntry) {
		    		i = 0;
		    		session.getTransaction().commit(); 
		    		session.clear();
		    		session.beginTransaction();
		    	}
				Eplan eplan = new Eplan(nextLine[0], nextLine[2], nextLine[3], nextLine[1], commit, CHANGE_TYPE.INSERT.ordinal());
				session.save(eplan);
		    }
    		session.getTransaction().commit(); 
		    reader.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Propagating the insert using sql query
	 * 
	 * @param commit
	 */
	private void propagateInsert(Integer commit) {
		String qEplanToVcdm = Helper.EPLAN_TO_VCDM_INSERT.concat(" WHERE commit_id = "+commit
				+" AND change_type = "+CHANGE_TYPE.INSERT.ordinal());
		executeQuery(qEplanToVcdm);
		String qVcdmToOpm = Helper.VCDM_TO_OPM_INSERT.concat(" WHERE commit_id = "+commit
				+" AND change_type = "+CHANGE_TYPE.INSERT.ordinal());
		executeQuery(qVcdmToOpm);
	}
	
	/**
	 * updating process
	 * 	we also count the time and print it out.
	 * 
	 * @param commit
	 * @param filename
	 */
	private long doUpdate(Integer commit, String filename) {

		System.out.println(filename);
		long starttime = System.currentTimeMillis();
		doUpdateEplan(commit, filename);
		long endtime = System.currentTimeMillis();
		long s = endtime - starttime;
		return s;
	}
	
	/**
	 * the update and also the propagation process.
	 * 	I am not using any sql since it quite complicated
	 * 	So, I am relying on the hibernate for doing the update
	 * 
	 * @param commit
	 * @param filename
	 */
	private void doUpdateEplan(Integer commit, String filename) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			CSVReader reader = new CSVReader(new FileReader(filename), ';', '\'', 1);
			String [] nextLine;
			int i = 0;
		    while ((nextLine = reader.readNext()) != null) {
		    	i++;
		    	if(i > maxEntry) {
		    		i = 0;
		    		session.getTransaction().commit(); 
		    		session.clear();
		    		session.beginTransaction();
		    	}
		    	
		    	Eplan eplan = (Eplan) session.get(Eplan.class, nextLine[0]);
		    	Vcdm vcdm = (Vcdm) session.get(Vcdm.class, nextLine[0]);
		    	Opm opm = (Opm) session.get(Opm.class, nextLine[0]);
                
                EplanChanges eplanChanges = new EplanChanges(eplan);
                VcdmChanges vcdmChanges = new VcdmChanges(vcdm);
                OpmChanges opmChanges = new OpmChanges(opm);
		    	
				eplan.setAttributes(nextLine[0], nextLine[2], nextLine[3], nextLine[1], commit, CHANGE_TYPE.UPDATE.ordinal());
				vcdm.setAttributes(eplan);
				opm.setAttributes(vcdm);
		    	
				session.save(eplan);
				session.save(vcdm);
				session.save(opm);
				session.save(eplanChanges);
				session.save(vcdmChanges);
				session.save(opmChanges);
		    }
    		session.getTransaction().commit(); 
		    reader.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * the deletion process
	 * 	including time counter
	 * 
	 * @param commit
	 * @param filename
	 */
	private long doDelete(Integer commit, String filename) {

		System.out.println(filename);
		long starttime = System.currentTimeMillis();
		doDeleteEplan(commit, filename);
		long endtime = System.currentTimeMillis();
		long s = endtime - starttime;
		return s;
	}
	
	/**
	 * similar to update, here you can find all the deletion and propagation
	 * 	I suppose the performance will be better using sql, but it need to be proved later
	 * 
	 * @param commit
	 * @param filename
	 */
	private void doDeleteEplan(Integer commit, String filename) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			CSVReader reader = new CSVReader(new FileReader(filename), ';', '\'', 1);
			String [] nextLine;
			int i = 0;
		    while ((nextLine = reader.readNext()) != null) {
		    	i++;
		    	if(i > maxEntry) {
		    		i = 0;
		    		session.getTransaction().commit(); 
		    		session.clear();
		    		session.beginTransaction();
		    	}
		    	Eplan eplan = (Eplan) session.get(Eplan.class, nextLine[0]);
		    	if(eplan==null) continue;
		    	Vcdm vcdm = (Vcdm) session.get(Vcdm.class, nextLine[0]);
		    	Opm opm = (Opm) session.get(Opm.class, nextLine[0]);
				
		    	EplanChanges eplanChanges = new EplanChanges(eplan);
		    	VcdmChanges vcdmChanges = new VcdmChanges(vcdm);
		    	OpmChanges opmChanges = new OpmChanges(opm);

                eplan.setAttributes(nextLine[0], nextLine[2], nextLine[3], nextLine[1], commit, CHANGE_TYPE.DELETE.ordinal());
                vcdm.setAttributes(eplan);
                opm.setAttributes(vcdm);

		    	EplanChanges eplanDel = new EplanChanges(eplan);
		    	VcdmChanges vcdmDel = new VcdmChanges(vcdm);
		    	OpmChanges opmDel = new OpmChanges(opm);
		    	
				session.delete(eplan);
				session.delete(vcdm);
				session.delete(opm);
				
				session.save(eplanChanges);
				session.save(vcdmChanges);
				session.save(opmChanges);
				
				session.save(eplanDel);
				session.save(vcdmDel);
				session.save(opmDel);
		    }
    		session.getTransaction().commit(); 
		    reader.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Native query execution
	 * 
	 * @param query
	 */
	private void executeQuery(String query) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery insQuery = session.createSQLQuery(query);
		insQuery.executeUpdate();
		session.getTransaction().commit();
	}

	/**
	 *  set heap limit (50000?)
	 *  open folder
	 *  iterate file per scenario
	 *  each scenario: inserts, updates, deletes
	 *  	print out time for each process
	 *  		breakdown for propagation
	 *  	print out volume of changeset
	 *  		breakdown for changeset
	 *  print out time for all process
	 */
	public static void main(String[] args) {
		RMoMachineV4 machine = new RMoMachineV4();
		machine.start();
	}	
}
